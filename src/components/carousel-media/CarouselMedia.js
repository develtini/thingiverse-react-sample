import React, { Component } from "react";

import "./style.scss";

import data from "./sample";

class CarouselMedia extends Component {
	render() {
		// const itemsToRender = data;

		// var imgDisplay = data.sizes.filter(v => v.sizes === "display");

		return (
			<div className="carousel-media">
				{data.map((item, index) => (
					<img alt={item.name} src={item.sizes} />
					// <p>{item.sizes}</p>
				))}
			</div>
			// var res = data.filter(v => v.location.country === 'USA');

			// <img src={small} srcSet={`${small} 300w, ${medium} 768w, ${large} 1280w`} />

			// image.filter(v => v.type === 'display')

			// 	<Carousel autoplay>
			// 	<div>
			// 		<h3>1</h3>
			// 	</div>
			// 	<div>
			// 		<h3>2</h3>
			// 	</div>
			// 	<div>
			// 		<h3>3</h3>
			// 	</div>
			// 	<div>
			// 		<h3>4</h3>
			// 	</div>
			// </Carousel>
		);
	}
}
export default CarouselMedia;

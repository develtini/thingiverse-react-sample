import React from "react";
// import { Switch, Route, NavLink } from "react-router-dom";
import { Switch, Route } from "react-router-dom";

import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

import Footer from "../layouts/footer";
import Header from "../layouts/header";

import { GridCardView } from "../grid-card-view";
import { ThingView } from "../item-view";
import Notfound from "./NotFound";

const useStyles = makeStyles(theme => ({
	cardGrid: {
		paddingTop: theme.spacing(8),
		paddingBottom: theme.spacing(8)
	},
	ppalContainer: {
		minHeight: "900px"
	}
}));

export default function App() {
	const classes = useStyles();

	return (
		<React.Fragment>
			<CssBaseline />
			<Header />
			<main>
				<Container className={classes.cardGrid} maxWidth="lg">
					<Grid className={classes.ppalContainer} container spacing={4}>
						<Switch>
							<Route exact path="/" component={GridCardView} />
							<Route exact path="/featured" component={GridCardView} />
							<Route
								exact
								path="/thing/:id"
								render={({ match }) => {
									return <ThingView id={match.params.id} />;
								}}
							/>
							<Route component={Notfound} />
						</Switch>
					</Grid>
				</Container>
			</main>
			<Footer />
		</React.Fragment>
	);
}

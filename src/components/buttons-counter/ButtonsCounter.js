import React, { Component } from "react";
import PropTypes from "prop-types";

import "./style.scss";

import { Icon } from "antd";
const data = {
	item: [
		{
			icon: "heart",
			text: "Like",
			counter: "34",
			url: "#"
		},
		{
			icon: "container",
			text: "Collect",
			counter: "18",
			url: "#"
		},
		{
			icon: "message",
			text: "Comments",
			counter: "0",
			url: "#"
		},
		{
			icon: "edit",
			text: "Post a Make",
			counter: "0",
			url: "#"
		},
		{
			icon: "eye",
			text: "Watch",
			counter: "0",
			url: "#"
		},
		{
			icon: "swap",
			text: "Remix It",
			counter: "0",
			url: "#"
		},
		{
			icon: "share-alt",
			text: "Share",
			counter: "0",
			url: "#"
		}
	]
};

class ButtonsCounter extends Component {
	render() {
		const itemsToRender = data.item;

		return (
			// <div>{console.log(data.item)}</div>
			// <Row justify="center">
			// <Col span={24}>
			<div className="buttons-counter">
				{itemsToRender.map((item, index) => (
					<React.Fragment>
						<a href={item.url}>
							<Icon className="icon" type={item.icon} />
							<span className="text">{item.text}</span>
							<span className="counter">{item.counter}</span>
						</a>
						<div className="divider" />
					</React.Fragment>
				))}
			</div>
		);
	}
}
ButtonsCounter.propTypes = {
	icon: PropTypes.string,
	text: PropTypes.string,
	counter: PropTypes.string,
	url: PropTypes.string
};

// export default withStyles(styles)(ItemView);
export default ButtonsCounter;

// 		// 	</Col>
// 		// 	<Col span={24}>
// 		// 		<Button type="link" block icon="search">
// 		// 			Collected
// 		// 		</Button>
// 		// 		<Divider />
// 		// 	</Col>
// 		// 	<Col span={24}>
// 		// 		<Button type="link" block icon="search">
// 		// 			Comments
// 		// 		</Button>
// 		// 		<Divider />
// 		// 	</Col>
// 		// 	<Col span={24}>
// 		// 		<Button type="link" block icon="search">
// 		// 			Post a Make
// 		// 		</Button>
// 		// 		<Divider />
// 		// 	</Col>
// 		// 	<Col span={24}>
// 		// 		<Button type="link" block icon="search">
// 		// 			Remix It
// 		// 		</Button>
// 		// 	</Col>
// 		// </Row>

import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { withStyles } from "@material-ui/styles";
import { NavLink } from "react-router-dom";

// import CameraIcon from '@material-ui/icons/PhotoCamera';

const styles = theme => ({
	headerItem: {
		// backgroundColor: theme.palette.background.paper,
		// padding: theme.spacing(6),
		color: "white",
		padding: "10px 8px",
		borderBottomColor: "rgb(255, 255, 255)",
		borderBottomStyle: "solid",
		borderBottomWidth: "2px"
	}
});

class Header extends Component {
	render() {
		const { classes } = this.props;

		return (
			<AppBar position="relative">
				<Toolbar>
					{/* <CameraIcon className={classes.icon} /> */}
					<Typography variant="h6" color="inherit" noWrap>
						Thingiverse Clone
					</Typography>
					<nav>
						        
						<NavLink className={classes.headerItem} to="/featured">
							FEATURED
						</NavLink>
					</nav>
				</Toolbar>
			</AppBar>
		);
	}
}

export default withStyles(styles)(Header);

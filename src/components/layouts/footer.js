import React, { Component } from "react";

import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/styles";
import Link from "@material-ui/core/Link";

const styles = theme => ({
	footer: {
		// backgroundColor: theme.palette.background.paper,
		// padding: theme.spacing(6),
		backgroundColor: "#3F51B0",
		padding: "20px",
		color: "white !important"
		// boxShadow:
		// 	"0px 2px -1px 4px rgba(0,0,0,0.2), 0px 4px 0px 5px rgba(0,0,0,0.14), 0px 1px 1px 10px rgba(0,0,0,0.12)"
	},
	footerTitle: {
		color: "white"
	}
});

class Footer extends Component {
	render() {
		const { classes } = this.props;

		return (
			<footer className={classes.footer}>
				<Typography
					variant="h6"
					className={classes.footerTitle}
					align="center"
					gutterBottom
				>
					Thingiverse Clone
				</Typography>
				<Typography variant="subtitle1" align="center" component="p">
					Clone of website Thingiverse to learn React with Material & usage of
					API to learn Apollo Graphql
				</Typography>
				<Copyright />
			</footer>
		);
	}
}

function Copyright() {
	return (
		<Typography variant="body2" align="center">
			{"Copyright © "}
			<Link color="inherit" rel="nofollow" href="https://www.thingiverse.com/">
				Thingiverse
			</Link>
			{". "}
			<Link
				rel="nofollow"
				color="inherit"
				href="https://www.thingiverse.com/legal/api"
			>
				Legal Terms
			</Link>{" "}
			{new Date().getFullYear()}
			{"."}
		</Typography>
	);
}

export default withStyles(styles)(Footer);

// import { makeStyles } from '@material-ui/core/styles';
// Version Theme Function Mode without Components
// const useStyles = makeStyles(theme => ({
// 	footer: {
// 		backgroundColor: theme.palette.background.paper,
// 		padding: theme.spacing(6),
// 	},
// }));
// const classes = useStyles();
//
// export default Footer;

import React, { Component } from "react";

// import { Query } from "react-apollo";
// import gql from "graphql-tag";
import { withStyles } from "@material-ui/styles";

import {
	Avatar,
	Row,
	Col,
	Layout,
	PageHeader,
	// Carousel,
	Button,
	Divider
} from "antd";

import { ButtonsCounter } from "../buttons-counter";
import "./style.css"; // Import regular stylesheet
// import "./style.scss";

import dataItem from "./sampleData.json";
import CarouselMedia from "../carousel-media/CarouselMedia";

// const { Content, Section } = Layout;
const { Content } = Layout;

// const FEED_QUERY = gql`
// 	query {
// 		getItem(id: 5) {
// 			name
// 			description
// 			instructions
// 		}
// 	}
// `;

const styles = theme => ({
	// root: {
	//   display: 'flex',
	//   flexWrap: 'wrap',
	//   justifyContent: 'space-around',
	//   overflow: 'hidden',
	//   // backgroundColor: theme.palette.background.paper,
	// },

	gridList: {
		width: 500,
		height: 450
	},
	icon: {
		color: "rgba(255, 255, 255, 0.54)"
	},
	paper: {
		height: 140,
		width: 100
	}
});

const breadRoutes = [
	{
		path: "popular",
		breadcrumbName: "Popular"
	}
];

class ItemView extends Component {
	render() {
		// const { classes } = this.props;
		return (
			<Layout>
				<Content>
					<PageHeader title={dataItem.name} breadcrumb={{ breadRoutes }} />
					<Layout>
						{/* TITLE */}
						<p>{dataItem.name}</p>
						<Avatar src={dataItem.creator.thumbnail} />
						<a href={dataItem.added}>{dataItem.creator.name}</a>
						<p>{dataItem.added}</p>

						<Row>
							<Col xs={24} md={16}>
								<CarouselMedia />
							</Col>
							<Col xs={24} md={8}>
								<Button type="primary" icon="search">
									DOWNLOAD ALL FILES
								</Button>
								<ButtonsCounter dataItem={dataItem} />
							</Col>
						</Row>
					</Layout>

					<Layout>
						<Row>
							<Col xs={24} md={8}>
								SideLeft
							</Col>
							<Col xs={24} md={16}>
								<p>Summary</p>
								<Divider />
								{dataItem.description}
								<p>Instructions</p>
								<Divider />
								{dataItem.instructions}
							</Col>
						</Row>
					</Layout>

					{/* <Grid container spacing={5} className={classes.mainGrid}>
						<Grid item xs={12} md={8}>
							<Typography variant="h6" gutterBottom />
							<Divider />
						</Grid>
						<Grid item xs={12} md={4}>
							<Paper elevation={0} className={classes.sidebarAboutBox}>
								<Typography variant="h6" gutterBottom>
									About
								</Typography>
								<Typography>
									Etiam porta sem malesuada magna mollis euismod. Cras
									mattis consectetur purus sit amet fermentum. Aenean
									lacinia bibendum nulla sed consectetur.
								</Typography>
							</Paper>
						</Grid>
					</Grid> */}
				</Content>
			</Layout>
		);

		// return (
		// 	<Query query={FEED_QUERY}>
		// 		{({ loading, error, data }) => {
		// 			if (loading) return <div>Fetching</div>;
		// 			if (error) return <div>Error: {error.message}</div>;
		// 			const dataItem = data.getItem;

		// 		}}
		// 	</Query>
		// );
	}
}

export default withStyles(styles)(ItemView);

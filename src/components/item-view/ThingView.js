import React, { Component } from "react";
import PropTypes from "prop-types";

import { Query } from "react-apollo";
import gql from "graphql-tag";
import { withStyles } from "@material-ui/styles";

import { withApollo } from "react-apollo";

// Front
import Grid from "@material-ui/core/Grid";
import LinkMaterial from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Divider from "@material-ui/core/Divider";
import Avatar from "@material-ui/core/Avatar";
import CircularProgress from "@material-ui/core/CircularProgress";
import ReportProblemIcon from "@material-ui/icons/ReportProblem";

// import data from "./samplething.json";
// import './style.css'; // Import regular stylesheet

import ButtonGroupActionItem from "./components/ButtonGroupActionItem";
import TabToggleInfoItem from "./components/TabToggleInfoItem";
import Carousel from "./components/Carousel";

let optionsDate = { year: "numeric", month: "long", day: "numeric" };

const FETH_QUERY = gql`
	query getItem($id: ID!) {
		getItem(id: $id) {
			name
			creator {
				name
				thumbnail
			}
			like_count
			collect_count
			description_html
			download_count
			view_count
			added
		}
	}
`;

const styles = theme => ({
	gridList: {
		width: 500,
		height: 450
	},
	icon: {
		color: "rgba(255, 255, 255, 0.54)"
	},
	center: {
		textAlign: "center"
	},
	avatar: {
		margin: 10,
		width: 60,
		height: 60
	},
	reportMsg: {
		marginLeft: "-6px"
	}
});

class ThingView extends Component {
	render() {
		const { classes } = this.props;
		return (
			<Query query={FETH_QUERY} variables={{ id: this.props.id }}>
				{({ loading, error, data }) => {
					if (loading)
						return (
							<Grid
								container
								spacing={0}
								direction="column"
								alignItems="center"
								justify="center"
								style={{ minHeight: "30vh" }}
							>
								<Grid item xs={3}>
									<CircularProgress />
									<div className={classes.reportMsg}>Loading</div>
								</Grid>
							</Grid>
						);
					if (error)
						return (
							<Grid
								container
								spacing={0}
								direction="column"
								alignItems="center"
								justify="center"
								style={{ minHeight: "30vh" }}
							>
								<Grid item xs={3}>
									<ReportProblemIcon />
									<div>Error: {error.message}</div>
								</Grid>
							</Grid>
						);

					const thing = data.getItem;
					let dateBrute = new Date(thing.added);
					const dateAdded = dateBrute.toLocaleDateString("en-us", optionsDate);

					return (
						<React.Fragment>
							<Grid item xs={12}>
								<Typography variant="h4" component="h1" gutterBottom>
									{thing.name}
								</Typography>
								<Divider />
								<Box
									display="flex"
									flexDirection="row"
									alignItems="center"
									my={1}
								>
									<Avatar
										alt={thing.creator.name}
										src={thing.creator.thumbnail}
										className={classes.avatar}
									/>
									<Typography className={classes.username}>
										By{" "}
										<LinkMaterial href={thing.creator.public_url}>
											{thing.creator.name}
										</LinkMaterial>
										<div>{dateAdded}</div>
									</Typography>
								</Box>
							</Grid>
							<Grid className={classes.center} item xs={12} md={8}>
								<Carousel id={this.props.id} />
							</Grid>
							<Grid className={classes.center} item xs={12} md={4}>
								<ButtonGroupActionItem data={thing} />
							</Grid>
							<Grid className={classes.center} item xs={12}>
								<TabToggleInfoItem data={thing} id={this.props.id} />
							</Grid>
						</React.Fragment>
					);
				}}
			</Query>
		);
	}
}

ThingView.propTypes = {
	id: PropTypes.string.isRequired
};

export default withApollo(withStyles(styles)(ThingView));

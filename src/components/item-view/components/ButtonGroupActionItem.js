import React, { Component } from "react";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import FavoriteIcon from "@material-ui/icons/Favorite";
import CollectionsIcon from "@material-ui/icons/Collections";
import ModeCommentIcon from "@material-ui/icons/ModeComment";
import BuildIcon from "@material-ui/icons/Build";
import EyeIcon from "@material-ui/icons/RemoveRedEye";
import ShareIcon from "@material-ui/icons/Share";

import { withStyles } from "@material-ui/styles";

const styles = theme => ({
	buttonGroupVertical: {
		alignItems: "flex-start",
		flexDirection: "column",
		justifyContent: "center",
		maxWidth: "400px"
	},
	buttonGroupItem: {
		borderColor: "rgba(0, 0, 0, 0.23) !important",
		borderStyle: "solid !important",
		borderWidth: "1px !important",
		borderRadius: "4px !important",
		marginBottom: "2px",
		width: "100%"
	},
	buttonGroupBox: {
		display: "flex",
		flexWrap: "wrap",
		width: "100%"
	},
	buttonGroupIcon: {
		width: "20%"
	},
	buttonGroupNum: {
		width: "20%",
		textAlign: "center"
	},
	buttonGroupText: {
		width: "60%",
		textAlign: "center"
	},
	blockButton: {
		width: "100%"
	}
});

class ButtonGroupActionItem extends Component {
	render() {
		const { classes } = this.props;
		return (
			<React.Fragment>
				<Box my={4}>
					<Button
						className={classes.blockButton}
						color="primary"
						variant="contained"
					>
						DOWNLOAD ALL FILES
					</Button>
				</Box>

				<ButtonGroup
					aria-label="Actions over item"
					className={classes.buttonGroupVertical}
				>
					<Box my={1}>
						<Button className={classes.buttonGroupItem}>
							<span className={classes.buttonGroupBox}>
								<FavoriteIcon
									color="action"
									className={classes.buttonGroupIcon}
								/>
								<span className={classes.buttonGroupText}>Like</span>
								<span className={classes.buttonGroupNum}>
									{this.props.data.like_count}
								</span>
							</span>
						</Button>
					</Box>
					<Box my={1}>
						<Button className={classes.buttonGroupItem}>
							<span className={classes.buttonGroupBox}>
								<CollectionsIcon
									color="action"
									className={classes.buttonGroupIcon}
								/>
								<span className={classes.buttonGroupText}>Collect</span>
								<span className={classes.buttonGroupNum}>
									{this.props.data.collect_count}
								</span>
							</span>
						</Button>
					</Box>
					<Box my={1}>
						<Button className={classes.buttonGroupItem}>
							<span className={classes.buttonGroupBox}>
								<ModeCommentIcon
									color="action"
									className={classes.buttonGroupIcon}
								/>
								<span className={classes.buttonGroupText}>Comments</span>
								<span className={classes.buttonGroupNum}>3333</span>
								{/*	{this.props.data.like_count}
								</span> */}
							</span>
						</Button>
					</Box>
					<Box my={1}>
						<Button className={classes.buttonGroupItem}>
							<span className={classes.buttonGroupBox}>
								<BuildIcon color="action" className={classes.buttonGroupIcon} />
								<span className={classes.buttonGroupText}>Post a make</span>
								<span className={classes.buttonGroupNum}>3333</span>
							</span>
						</Button>
					</Box>
					<Box my={1}>
						<Button className={classes.buttonGroupItem}>
							<span className={classes.buttonGroupBox}>
								<EyeIcon color="action" className={classes.buttonGroupIcon} />
								<span className={classes.buttonGroupText}>Watch</span>
								<span className={classes.buttonGroupNum}>3333</span>
							</span>
						</Button>
					</Box>
					<Box my={1}>
						<Button className={classes.buttonGroupItem}>
							<span className={classes.buttonGroupBox}>
								<ShareIcon color="action" className={classes.buttonGroupIcon} />
								<span className={classes.buttonGroupText}>Share</span>
								<span className={classes.buttonGroupNum}>3333</span>
							</span>
						</Button>
					</Box>
				</ButtonGroup>
			</React.Fragment>
		);
	}
}

export default withStyles(styles)(ButtonGroupActionItem);

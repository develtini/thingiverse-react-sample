import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import MobileStepper from "@material-ui/core/MobileStepper";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { graphql } from "react-apollo";
import { withApollo } from "react-apollo";

import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";

import data from "./sampleCarousel.json";

const FETH_QUERY = gql`
	query getItemImages($id: ID!) {
		getItemImages(id: $id) {
			name
			creator {
				name
				thumbnail
			}
		}
	}
`;

const useStyles = makeStyles(theme => ({
	root: {
		width: "100%",
		flexGrow: 1
	},
	header: {
		display: "flex",
		alignItems: "center",
		height: 50,
		paddingLeft: theme.spacing(4),
		backgroundColor: theme.palette.background.default
	},
	img: {
		maxHeight: 600,
		// maxWidth: 480,
		overflow: "hidden",
		display: "block",
		width: "100%"
	}
}));

// function DogPhoto({ id }) {
// 	const { loading, error, data } = useQuery(FETH_QUERY, {
// 		variables: { îd }
// 	});

// 	if (loading) return null;
// 	if (error) return `Error! ${error}`;

// 	return (
// 		<img src={data.dog.displayImage} style={{ height: 100, width: 100 }} />
// 	);
// }

export default function Carousel({ id }) {
	const classes = useStyles();
	const theme = useTheme();
	const [activeStep, setActiveStep] = React.useState(0);

	console.log("PROPS");
	console.log({ id });
	console.log("####");

	function handleNext() {
		setActiveStep(prevActiveStep => prevActiveStep + 1);
	}

	function handleBack() {
		setActiveStep(prevActiveStep => prevActiveStep - 1);
	}

	const { loading, error, data } = useQuery(FETH_QUERY, {
		variables: { id: id }
	});

	if (loading) return "Loading...";
	if (error) return `Error! ${error.message}`;

	const maxSteps = data.length;

	return (
		<div className={classes.root}>
			{/* <Paper square elevation={0} className={classes.header}>
				<Typography>{data[activeStep].name}</Typography>
			</Paper> */}
			<MobileStepper
				steps={maxSteps}
				position="static"
				variant="text"
				activeStep={activeStep}
				nextButton={
					<Button
						size="small"
						onClick={handleNext}
						disabled={activeStep === maxSteps - 1}
					>
						Next
						{theme.direction === "rtl" ? (
							<KeyboardArrowLeft />
						) : (
							<KeyboardArrowRight />
						)}
					</Button>
				}
				backButton={
					<Button size="small" onClick={handleBack} disabled={activeStep === 0}>
						{theme.direction === "rtl" ? (
							<KeyboardArrowRight />
						) : (
							<KeyboardArrowLeft />
						)}
						Back
					</Button>
				}
			/>
			{/* <img
				className={classes.img}
				src={
					data[activeStep]["sizes"].find(
						d => d.size === "medium" && d.type === "display"
					).url
				}
				alt={data[activeStep].name}
			/> */}
			<img
				alt={data[activeStep].name}
				className={classes.img}
				src={
					data[activeStep]["sizes"].find(
						d => d.size === "small" && d.type === "display"
					).url
				}
				srcSet={`${
					data[activeStep]["sizes"].find(
						d => d.size === "small" && d.type === "display"
					).url
				} 300w, ${
					data[activeStep]["sizes"].find(
						d => d.size === "medium" && d.type === "display"
					).url
				} 768w, ${
					data[activeStep]["sizes"].find(
						d => d.size === "large" && d.type === "display"
					).url
				} 1280w`}
			/>
		</div>
	);
}

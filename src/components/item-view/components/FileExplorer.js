import React, { Component } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import { withStyles } from "@material-ui/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

import { withApollo } from "react-apollo";

// import data from "./sampleFile.json";
import { Typography } from "@material-ui/core";

const styles = theme => ({
	root: {
		width: "100%",
		// marginTop: theme.spacing(3),
		marginTop: "3px",
		overflowX: "auto"
	},
	table: {
		minWidth: 650
	},
	rightPane: {
		marginLeft: "16px",
		marginTop: "24px"
	}
});

const FETH_QUERY = gql`
	query getItemFiles($id: ID!) {
		getItemFiles(id: $id) {
			name
			size
			thumbnail
			download_url
			default_image {
				name
				sizes {
					type
					size
					url
				}
			}
		}
	}
`;

class FileExplorer extends Component {
	render() {
		const { classes } = this.props;

		return (
			<Query query={FETH_QUERY} variables={{ id: this.props.id }}>
				{({ loading, error, data }) => {
					if (loading)
						return (
							<Grid
								container
								spacing={0}
								direction="column"
								alignItems="center"
								justify="center"
								style={{ minHeight: "30vh" }}
							>
								<Grid item xs={3}>
									<CircularProgress />
									<div>Loading</div>
								</Grid>
							</Grid>
						);
					if (error) return <div>Error: {error.message}</div>;
					const files = data.getItemFiles;
					if (files === undefined) return <div>Not Data Found</div>;

					return (
						<Grid container>
							{/* PANE LEFT */}
							<Grid item xs={12} md={9}>
								<Paper className={classes.root}>
									<Table className={classes.table}>
										<TableHead>
											<TableRow>
												<TableCell>Image</TableCell>
												<TableCell>Filename</TableCell>
												<TableCell align="right">Downloads</TableCell>
												<TableCell align="right">Size</TableCell>
											</TableRow>
										</TableHead>
										<TableBody>
											{files.map(row => (
												<TableRow key={row.name}>
													<TableCell align="left">
														<img src={row.thumbnail} alt={row.name} />
													</TableCell>
													<TableCell component="th" scope="row">
														<Link href={row.download_url}>{row.name}</Link>
													</TableCell>
													<TableCell align="right">xxxx</TableCell>
													<TableCell align="right">{row.size}</TableCell>
												</TableRow>
											))}
										</TableBody>
									</Table>
								</Paper>
							</Grid>
							{/* PANE RIGHT */}
							<Grid item xs={12} md={3}>
								<div className={classes.rightPane}>
									<Typography>
										Flying Sea Turtle by amaochan is licensed under the Creative
										Commons - Attribution - Non-Commercial - No Derivatives
										license.
									</Typography>
									<Typography>
										By downloading this thing, you agree to abide by the
										license: Creative Commons - Attribution - Non-Commercial -
										No Derivatives
									</Typography>
								</div>
							</Grid>
						</Grid>
					);
				}}
			</Query>
		);
	}
}

export default withApollo(withStyles(styles)(FileExplorer));

import React from "react";
import PropTypes from "prop-types";
// import SwipeableViews from "react-swipeable-views";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import EyeIcon from "@material-ui/icons/RemoveRedEye";
import FolderIcon from "@material-ui/icons/Folder";
import BuildIcon from "@material-ui/icons/Build";
import AppsIcon from "@material-ui/icons/Apps";
import ModeCommentIcon from "@material-ui/icons/ModeComment";
import CollectionsIcon from "@material-ui/icons/Collections";
import SwapCallsIcon from "@material-ui/icons/SwapCalls";

import ItemDetails from "./ItemDetails";
import FileExplorer from "./FileExplorer";

function TabPanel(props) {
	const { children, value, index, ...other } = props;

	return (
		<Typography
			component="div"
			role="tabpanel"
			hidden={value !== index}
			id={`action-tabpanel-${index}`}
			aria-labelledby={`action-tab-${index}`}
			{...other}
		>
			<Box p={3}>{children}</Box>
		</Typography>
	);
}

TabPanel.propTypes = {
	children: PropTypes.node,
	index: PropTypes.any.isRequired,
	value: PropTypes.any.isRequired
};

function a11yProps(index) {
	return {
		id: `action-tab-${index}`,
		"aria-controls": `action-tabpanel-${index}`
	};
}

const useStyles = makeStyles(theme => ({
	root: {
		backgroundColor: theme.palette.background.paper,
		width: "100%",
		position: "relative",
		minHeight: 200
	}
}));

export default function TabToggleInfoItem({ id, data }) {
	const classes = useStyles();
	const theme = useTheme();
	const [value, setValue] = React.useState(0);
	const thing = data;

	function handleChange(event, newValue) {
		setValue(newValue);
	}

	function handleChangeIndex(index) {
		setValue(index);
	}

	return (
		<div className={classes.root}>
			<AppBar position="static" color="default">
				<Tabs
					value={value}
					onChange={handleChange}
					indicatorColor="primary"
					textColor="primary"
					aria-label="action tabs example"
				>
					<Tab icon={<EyeIcon />} label="Thing Details" {...a11yProps(0)} />
					<Tab icon={<FolderIcon />} label="Thing Files" {...a11yProps(1)} />
					<Tab icon={<AppsIcon />} label="Apps" {...a11yProps(2)} />
					<Tab icon={<ModeCommentIcon />} label="Comments" {...a11yProps(3)} />
					<Tab icon={<BuildIcon />} label="Makes" {...a11yProps(4)} />
					<Tab
						icon={<CollectionsIcon />}
						label="Collections"
						{...a11yProps(5)}
					/>
					<Tab icon={<SwapCallsIcon />} label="Remixes" {...a11yProps(6)} />
				</Tabs>
			</AppBar>
			<div
				// axis={theme.direction === "rtl" ? "x-reverse" : "x"}
				index={value}
				onChangeIndex={handleChangeIndex}
			>
				<TabPanel value={value} index={0} dir={theme.direction}>
					<ItemDetails data={thing} />
				</TabPanel>
				<TabPanel value={value} index={1} dir={theme.direction}>
					<FileExplorer id={id} />
				</TabPanel>
				<TabPanel value={value} index={2} dir={theme.direction}>
					Item Three
				</TabPanel>
				<TabPanel value={value} index={3} dir={theme.direction}>
					Item 4
				</TabPanel>
				<TabPanel value={value} index={4} dir={theme.direction}>
					Item 5
				</TabPanel>
				<TabPanel value={value} index={5} dir={theme.direction}>
					Item 6
				</TabPanel>
				<TabPanel value={value} index={6} dir={theme.direction}>
					Item 7
				</TabPanel>
			</div>
		</div>
	);
}

TabToggleInfoItem.propTypes = {
	data: PropTypes.object
};

// BASED ON: https://material-ui.com/components/buttons/#floating-action-buttons

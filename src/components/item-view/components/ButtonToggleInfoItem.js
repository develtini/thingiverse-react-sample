import React, { Component } from "react";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Button from "@material-ui/core/Button";
import EyeIcon from "@material-ui/icons/RemoveRedEye";

import { withStyles } from "@material-ui/styles";

const styles = theme => ({
	buttonToggle: {
		margin: "1px"
	},
	rightIcon: {
		marginLeft: "1px"
	}
});

class ButtonToggleInfoItem extends Component {
	render() {
		const { classes } = this.props;

		return (
			<React.Fragment>
				<ButtonGroup aria-label="Content Toggle Button">
					<Button
						variant="contained"
						color="secondary"
						className={classes.buttonToggle}
					>
						Thing Details
						<EyeIcon className={classes.rightIcon} />
					</Button>
					<Button
						variant="contained"
						color="secondary"
						className={classes.buttonToggle}
					>
						Thing Details
						<EyeIcon className={classes.rightIcon} />
					</Button>
					<Button
						variant="contained"
						color="secondary"
						className={classes.buttonToggle}
					>
						Thing Details
						<EyeIcon className={classes.rightIcon} />
					</Button>
					<Button
						variant="contained"
						color="secondary"
						className={classes.buttonToggle}
					>
						Thing Details
						<EyeIcon className={classes.rightIcon} />
					</Button>
					<Button
						variant="contained"
						color="secondary"
						className={classes.buttonToggle}
					>
						Thing Details
						<EyeIcon className={classes.rightIcon} />
					</Button>
					<Button
						variant="contained"
						color="secondary"
						className={classes.buttonToggle}
					>
						Thing Details
						<EyeIcon className={classes.rightIcon} />
					</Button>
					<Button
						variant="contained"
						color="secondary"
						className={classes.buttonToggle}
					>
						Thing Details
						<EyeIcon className={classes.rightIcon} />
					</Button>
				</ButtonGroup>
			</React.Fragment>
		);
	}
}

export default withStyles(styles)(ButtonToggleInfoItem);

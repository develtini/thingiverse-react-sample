import React, { Component } from "react";

import { withStyles } from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";
import ReportIcon from "@material-ui/icons/Report";
import SystemUpdateIcon from "@material-ui/icons/SystemUpdateRounded";
import TimelineIcon from "@material-ui/icons/Timeline";
import Box from "@material-ui/core/Box";

const styles = theme => ({
	root: {
		// marginTop: "20px"
	},
	leftPane: {
		marginRight: "40px"
	},
	blockButton: {
		width: "100%",
		justifyContent: "initial"
	},
	iconButton: {
		marginRight: "5px"
	},
	staticsBox: {
		marginLeft: "16px",
		textAlign: "left"
	},
	staticNum: {
		marginLeft: "8px"
	},
	descriptionContent: {
		textAlign: "left"
	}
});

class ItemDetails extends Component {
	render() {
		const { classes } = this.props;
		const thing = this.props.data;

		return (
			<div className={classes.root}>
				<Grid container>
					{/* PANE LEFT */}
					<Grid item xs={12} md={4}>
						<div className={classes.leftPane}>
							{/* CONTENTS TITLE */}
							<Box mb={4}>
								<Typography align="left" variant="h6">
									Contents
								</Typography>
								<Divider m={8} />
							</Box>
							{/* BUTTON TIP */}
							<Box my={4}>
								<Button
									className={classes.blockButton}
									color="primary"
									variant="contained"
								>
									<MonetizationOnIcon className={classes.iconButton} />
									TIP DESIGNER
								</Button>
							</Box>
							{/* BUTTON REPORT */}
							<Box my={4}>
								<Button className={classes.blockButton} variant="contained">
									<ReportIcon className={classes.iconButton} />
									REPORT THING
								</Button>
							</Box>
							<Box mb={4}>
								<Typography align="left" variant="h6">
									Thing Details
								</Typography>
								<Divider />
								{/* VIEWS */}
								<div className={classes.staticsBox}>
									<TimelineIcon />
									<span className={classes.staticNum}>
										{thing.view_count} Views
									</span>
								</div>
								{/* DOWNLOAD */}
								<div className={classes.staticsBox}>
									<SystemUpdateIcon />
									<span className={classes.staticNum}>
										{thing.download_count} Downloads
									</span>
								</div>
								{/* FOUND IN ART */}
								{/* <div className={classes.staticsBox}>
								<SystemUpdateIcon />
								<Link className={classes.staticNum}>Found in Art</Link>
							</div> */}
							</Box>
						</div>
					</Grid>
					{/* PANE RIGHT */}
					<Grid className={classes.rightPane} item xs={12} md={8}>
						<Typography align="left" variant="h6">
							Summary
						</Typography>
						<Divider />
						<div
							className={classes.descriptionContent}
							dangerouslySetInnerHTML={{ __html: thing.description_html }}
						></div>
					</Grid>
				</Grid>
			</div>
		);
	}
}

export default withStyles(styles)(ItemDetails);

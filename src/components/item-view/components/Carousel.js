import React, { Component } from "react";
import MobileStepper from "@material-ui/core/MobileStepper";
import gql from "graphql-tag";
import { Query } from "react-apollo";
import { withStyles } from "@material-ui/styles";

import { withApollo } from "react-apollo";

import Button from "@material-ui/core/Button";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";

const FETH_QUERY = gql`
	query getItemImages($id: ID!) {
		getItemImages(id: $id) {
			name
			sizes {
				type
				size
				url
			}
		}
	}
`;

const styles = theme => ({
	root: {
		width: "100%",
		flexGrow: 1
	},
	header: {
		display: "flex",
		alignItems: "center",
		height: 50,
		// paddingLeft: theme.spacing(4),
		paddingLeft: "16px"
		// backgroundColor: theme.palette.background.default
	},
	img: {
		maxHeight: 600,
		// maxWidth: 480,
		overflow: "hidden",
		display: "block",
		width: "100%"
	}
});

class Carousel extends Component {
	constructor(props) {
		super(props);
		this.state = {
			activeStep: 0
		};
		this._handleNext = this._handleNext.bind(this);
		this._handleBack = this._handleBack.bind(this);
	}

	_handleNext() {
		this.setState({
			activeStep: this.state.activeStep + 1
		});
	}

	_handleBack() {
		this.setState({
			activeStep: this.state.activeStep - 1
		});
	}

	render() {
		const { classes } = this.props;

		return (
			<Query query={FETH_QUERY} variables={{ id: this.props.id }}>
				{({ loading, error, data }) => {
					if (loading)
						return (
							<Grid
								container
								spacing={0}
								direction="column"
								alignItems="center"
								justify="center"
								style={{ minHeight: "30vh" }}
							>
								<Grid item xs={3}>
									<CircularProgress />
									<div>Loading</div>
								</Grid>
							</Grid>
						);
					if (error)
						return (
							<Grid
								container
								spacing={0}
								direction="column"
								alignItems="center"
								justify="center"
								style={{ minHeight: "30vh" }}
							>
								<Grid item xs={3}>
									<div>Error: {error.message}</div>
								</Grid>
							</Grid>
						);
					const thing = data.getItemImages;
					const maxSteps = thing.length;

					return (
						<div className={classes.root}>
							{/* <Paper square elevation={0} className={classes.header}>
				<Typography>{thing[activeStep].name}</Typography>
			</Paper> */}
							<MobileStepper
								steps={maxSteps}
								position="static"
								variant="text"
								activeStep={this.state.activeStep}
								nextButton={
									<Button
										size="small"
										onClick={this._handleNext}
										disabled={this.state.activeStep === maxSteps - 1}
									>
										Next
										<KeyboardArrowRight />
									</Button>
								}
								backButton={
									<Button
										size="small"
										onClick={this._handleBack}
										disabled={this.state.activeStep === 0}
									>
										<KeyboardArrowLeft />
										Back
									</Button>
								}
							/>
							{/* SIMPLE IMG */}
							{/* <img
								className={classes.img}
								src={
									thing[this.state.activeStep]["sizes"].find(
										d => d.size === "medium" && d.type === "display"
									).url
								}
								alt={thing[this.state.activeStep].name}
								/> 
							*/}
							<img
								alt={thing[this.state.activeStep].name}
								className={classes.img}
								src={
									thing[this.state.activeStep]["sizes"].find(
										d => d.size === "small" && d.type === "display"
									).url
								}
								srcSet={`${
									thing[this.state.activeStep]["sizes"].find(
										d => d.size === "small" && d.type === "display"
									).url
								} 300w, ${
									thing[this.state.activeStep]["sizes"].find(
										d => d.size === "medium" && d.type === "display"
									).url
								} 768w, ${
									thing[this.state.activeStep]["sizes"].find(
										d => d.size === "large" && d.type === "display"
									).url
								} 1280w`}
							/>
						</div>
					);
				}}
			</Query>
		);
	}
}

export default withApollo(withStyles(styles)(Carousel));

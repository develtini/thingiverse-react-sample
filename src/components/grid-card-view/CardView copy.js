import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardActions from "@material-ui/core/CardActions";
import CardActionArea from "@material-ui/core/CardActionArea";
import Typography from "@material-ui/core/Typography";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge";
import CardContent from "@material-ui/core/CardContent";
import Grid from "@material-ui/core/Grid";
import Avatar from "@material-ui/core/Avatar";
import Link from "@material-ui/core/Link";

import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
	card: {
		maxWidth: 500
	},
	title: {},
	media: {
		height: 0,
		paddingTop: "75%" // 4:3
		// paddingTop: "56.25%", // 16:9
	},
	expand: {
		transform: "rotate(0deg)",
		marginLeft: "auto",
		transition: theme.transitions.create("transform", {
			duration: theme.transitions.duration.shortest
		})
	},
	expandOpen: {
		transform: "rotate(180deg)"
	},
	avatar: {
		// backgroundColor: red[500],
	},
	overlay: {
		position: "absolute",
		top: "20px",
		left: "20px",
		color: "black",
		backgroundColor: "white"
	}
});

class CardView extends Component {
	render() {
		const { classes } = this.props;

		return (
			<Card className={classes.card}>
				<CardActionArea>
					<Link to={this.props.link.url}>
						<CardMedia
							className={classes.media}
							image={this.props.link.spotlight_images.small}
							title="Paella dish"
						/>
						{/* {this.props.link.name} */}
					</Link>
				</CardActionArea>
				<CardContent>
					<Typography
						className={classes.title}
						color="textSecondary"
						variant="subtitle1"
						gutterBottom
					>
						{this.props.link.name}
					</Typography>
					<Grid container>
						<Avatar
							iten
							alt={this.props.link.creator.name}
							src={this.props.link.creator.thumbnail}
						/>
						<Typography
							align="center"
							className={classes.title}
							color="textSecondary"
							variant="caption"
						>
							By: {this.props.link.creator.name}
						</Typography>
					</Grid>
				</CardContent>

				<CardActions>
					{/* <Typography className={classes.title} color="textSecondary" gutterBottom>
					{this.props.link.name}
			</Typography> */}
					{/* <GridListTileBar
              title={this.props.name}
              subtitle={<span>by: {this.props.name}</span>}
              // actionIcon={
              //   <IconButton aria-label={`info about ${this.props.name}`} className={classes.icon}>
              //     <InfoIcon />
              //   </IconButton>
              // }
						/>			 */}
					<Grid container />
					<Badge className={classes.margin} badgeContent={4} color="primary">
						<IconButton align="end" size="small" aria-label="add to favorites">
							<FavoriteIcon />
						</IconButton>
					</Badge>
					<IconButton align="end" size="small" aria-label="share">
						<ShareIcon />
					</IconButton>
				</CardActions>
			</Card>
		);
	}
}

export default withStyles(styles)(CardView);

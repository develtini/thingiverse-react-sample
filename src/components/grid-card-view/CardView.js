import React, { Component } from "react";
import { Card, Icon, Avatar, Badge } from "antd";
import { Link } from "react-router-dom";

if (process.env.NODE_ENV === "production") {
	import("./CardView.css");
} else {
	// Make sure that the custom stylesheet overrides antd css.
	import("antd/dist/antd.css").then(() => import("./CardView.css"));
}
// import style from "./CardView.css";

const { Meta } = Card;

// const cardStyle = {
// 	height: 0,
// 	paddingTop: "75%"
// 	// width: 300;
// };

// const iconStyle = {
// 	fontSize: "24px"
// };

class CardView extends Component {
	render() {
		return (
			<Card
				// style={{ cardStyle }}
				hoverable
				cover={
					<img alt="example" src={this.props.link.spotlight_images.small} />
				}
				actions={[
					<Badge count={5}>
						{/* <a href="www.google.es"> */}
						<Icon style={{ fontSize: "24px" }} type="heart" />
						{/* </a> */}
					</Badge>,
					<Badge count={5}>
						<Icon style={{ fontSize: "24px" }} type="build" />
					</Badge>,
					<Badge count={5}>
						<Icon style={{ fontSize: "24px" }} type="message" />
					</Badge>
				]}
			>
				<Meta
					avatar={<Avatar src={this.props.link.creator.thumbnail} />}
					title={[
						<Link
							to={{
								pathname: `item/${this.props.link.id}`
							}}
						>
							{this.props.link.name}
						</Link>
					]}
					description={"By: " + this.props.link.creator.name}
				/>
			</Card>
		);
	}
}

export default CardView;

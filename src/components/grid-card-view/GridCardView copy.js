import React, { Component, Fragment, Link } from 'react'

import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import { CardView } from './CardView';
import { makeStyles } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/styles';

// Front
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';

import './style.css'; // Import regular stylesheet

const FEED_QUERY = gql`
  {
    featured {
			id
			url
			name
			thumbnail
			spotlight_images {
				small
				large
			}
			creator {
				name
				url
			}
    }
  }
`

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    // backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: 500,
    height: 450,
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
});


class GridCardView extends Component {


render() {
		const { classes } = this.props;
		return (
			<Query query={FEED_QUERY}>
				{({ loading, error, data }) => {
          if (loading) return <div>Fetching</div>
					if (error) return <div>Error: {error.message}</div> 					
					const linksToRender = data.featured

					return (						
						<div className={classes.root}>					
      			
      			<GridList cellHeight={180} className={classes.gridList}>

							{linksToRender.map((link, index) => (
								// <Link key={link.id} link={link} index={index} />
							// <CardView
								// 	key={link.id}
								// 	link={link}
								// 	index={index}
								// 	updateStoreAfterVote={this._updateCacheAfterVote}
								// />								
								<GridListTile key={link.img}>
								<img src={link.spotlight_images.large} alt={link.title} />
								<GridListTileBar
									title={link.name}
									subtitle={<span>by: {link.creator.name}</span>}
									actionIcon={
										// <IconButton aria-label={`info about ${link.title}`} className={classes.icon}>
										<IconButton aria-label={`info about ${link.title}`}>

											<InfoIcon />
										</IconButton>
									}
								/>
							</GridListTile>								
							))}
						</GridList>														
						</div>
					)					
				}}
			</Query>
		)		
	}
}


export default withStyles(styles)(GridCardView);

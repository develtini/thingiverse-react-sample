import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link as RouterLink } from "react-router-dom";

import { Query } from "react-apollo";
import gql from "graphql-tag";

import { withStyles } from "@material-ui/styles";

import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import LinkMaterial from "@material-ui/core/Link";
import CardMedia from "@material-ui/core/CardMedia";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import FavoriteIcon from "@material-ui/icons/Favorite";
import CollectionsIcon from "@material-ui/icons/Collections";
import ModeCommentIcon from "@material-ui/icons/ModeComment";
import CardActionArea from "@material-ui/core/CardActionArea";
import Avatar from "@material-ui/core/Avatar";
import CircularProgress from "@material-ui/core/CircularProgress";

// import items from "./sampleData.json";
// import ButtonBases from "./ButtonBases";

const FEED_QUERY = gql`
	{
		featured {
			id
			public_url
			name
			thumbnail
			spotlight_images {
				small
				large
			}
			creator {
				name
				public_url
				thumbnail
			}
		}
	}
`;

const styles = theme => ({
	card: {
		height: "100%",
		display: "flex",
		flexDirection: "column"
	},
	cardMedia: {
		paddingTop: "75%" // 4:3
		// paddingTop: '56.25%', // 16:9
	},
	cardContent: {
		flexGrow: 1
	},
	cardContentTitle: {
		flexGrow: 1,
		padding: "10px",
		// whiteSpace: "nowrap",
		// textOverflow: "ellipsis",
		// overflow: "hidden",
		minHeight: "91px"
	},
	cardContentUser: {
		display: "flex",
		flexWrap: "wrap",
		width: "100%",
		padding: "10px"
	},
	shareIcon: {
		marginRight: "12px"
	},
	avatar: {
		// margin: 10,
		// width: "33%"
	},
	username: {
		marginLeft: "20px",
		marginTop: "4px"
	},
	cardActions: {
		display: "flex",
		justifyContent: "space-around"
	},
	datePublish: {
		display: "block"
	}
});

class GridCardView extends Component {
	render() {
		const { classes } = this.props;
		return (
			<Query query={FEED_QUERY}>
				{({ loading, error, data }) => {
					if (loading)
						return (
							<Grid
								container
								spacing={0}
								direction="column"
								alignItems="center"
								justify="center"
								style={{ minHeight: "100vh" }}
							>
								<Grid item xs={3}>
									<CircularProgress />
									<div>Loading</div>
								</Grid>
							</Grid>
						);
					if (error)
						return (
							<Grid
								container
								spacing={0}
								direction="column"
								alignItems="center"
								justify="center"
								style={{ minHeight: "100vh" }}
							>
								<Grid item xs={3}>
									<div>Error: {error.message}</div>
								</Grid>
							</Grid>
						);
					const items = data.featured;
					return items.map(card => (
						<Grid item key={card.id} xs={12} sm={6} md={4}>
							<Card className={classes.card}>
								<CardActionArea
									component={RouterLink}
									onClick={event => {}}
									to={{
										pathname: `thing/${card.id}`
									}}
									disableRipple
								>
									<CardMedia
										className={classes.cardMedia}
										image={card.spotlight_images.small}
										title="Image title"
									/>
									<CardContent className={classes.cardContentTitle}>
										<Typography gutterBottom variant="h6" component="h2">
											{card.name}
										</Typography>
									</CardContent>
								</CardActionArea>
								<CardContent className={classes.cardContentUser}>
									<Avatar
										alt={card.creator.name}
										src={card.creator.thumbnail}
										className={classes.avatar}
									/>
									<Typography className={classes.username}>
										By{" "}
										<LinkMaterial href={card.creator.public_url}>
											{card.creator.name}

											{/* <Link
									to={{
										pathname: `thing/${card.id}`
									}}
								>
									{card.creator.name}
								</Link> */}
										</LinkMaterial>
										<span className={classes.datePublish}>Apr 30, 2019</span>
									</Typography>
								</CardContent>
								<CardActions className={classes.cardActions}>
									<Button className={classes.shareButton} size="small">
										<FavoriteIcon
											color="action"
											className={classes.shareIcon}
										/>
										0000
									</Button>
									<Button className={classes.shareButton} size="small">
										<CollectionsIcon
											color="action"
											className={classes.shareIcon}
										/>
										0000
									</Button>
									<Button className={classes.shareButton} size="small">
										<ModeCommentIcon
											color="action"
											className={classes.shareIcon}
										/>
										0000
									</Button>
								</CardActions>
							</Card>
						</Grid>
					));
				}}
			</Query>
		);
	}
}

GridCardView.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(GridCardView);

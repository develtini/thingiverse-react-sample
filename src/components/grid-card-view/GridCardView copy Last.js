import React, { Component, Fragment, Link } from "react";

import { Query } from "react-apollo";
import gql from "graphql-tag";
import { CardView } from "./index";
import { withStyles } from "@material-ui/styles";

// Front
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";

import { Card, Icon, Avatar } from "antd";

import { Container } from "@material-ui/core";

const { Meta } = Card;

// import './style.css'; // Import regular stylesheet

const FEED_QUERY = gql`
	{
		featured {
			id
			url
			name
			thumbnail
			spotlight_images {
				small
				large
			}
			creator {
				name
				public_url
				thumbnail
			}
		}
	}
`;

const styles = theme => ({});

class GridCardView extends Component {
	render() {
		const { classes } = this.props;
		return (
			<Query query={FEED_QUERY}>
				{({ loading, error, data }) => {
					if (loading)
						return (
							<Grid
								container
								spacing={0}
								direction="column"
								alignItems="center"
								justify="center"
								style={{ minHeight: "100vh" }}
							>
								<Grid item xs={3}>
									<CircularProgress />
									<div>Fetching</div>
								</Grid>
							</Grid>
						);
					if (error)
						return (
							<Grid
								container
								spacing={0}
								direction="column"
								alignItems="center"
								justify="center"
								style={{ minHeight: "100vh" }}
							>
								<Grid item xs={3}>
									<div>Error: {error.message}</div>;
								</Grid>
							</Grid>
						);
					const linksToRender = data.featured;

					return (
						<div className={classes.root}>
							{/* <GridList cellHeight={180} className={classes.gridList}> */}
							<Grid container justify="space-around" spacing={3}>
								{linksToRender.map((link, index) => (
									<Grid item xs={12} sm={4} md={3}>
										{/* <Paper className={classes.paper} /> */}

										<CardView
											key={link.id}
											link={link}
											index={index}
											// updateStoreAfterVote={this._updateCacheAfterVote}
										/>
									</Grid>
								))}
							</Grid>
						</div>
					);
				}}
			</Query>
		);
	}
}

export default withStyles(styles)(GridCardView);

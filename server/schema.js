const { gql } = require("apollo-server");

const typeDefs = gql`
	scalar DateTime

	type Query {
		features: [Item]
	}
	type Item {
		id: ID!
		name: String!
		public_url: String!
		url: String!
		thumbnail: String!
		images: Image
		creator: Author
		added: DateTime!
	}

	type Image {
		large: String!
		small: String!
	}

	type Author {
		name: String!
		thumbnail: String!
		public_url: String!
	}
`;

// product(id: Int!): Product

module.exports = typeDefs;

function comments(parent, args, { dataSources }) {
	return dataSources.ThingiverseAPI.getItemComments(parent.id);
}

function images(parent, args, { dataSources }) {
	return dataSources.ThingiverseAPI.getItemImages(parent.id);
}

module.exports = {
	comments,
	images
};

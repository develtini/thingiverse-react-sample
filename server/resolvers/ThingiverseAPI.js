const { RESTDataSource } = require("apollo-datasource-rest");

class ThingiverseAPI extends RESTDataSource {
	constructor() {
		super();
		this.baseURL = "https://api.thingiverse.com/";
	}

	willSendRequest(request) {
		request.headers.set(
			"Authorization",
			"Bearer 2e1e23cf95a1271a4f21d125d037b5be"
		);
	}

	async featured() {
		return this.get(`featured`);
	}

	async getItem(id) {
		return this.get(`things/${id}`);
	}

	async getItemImages(id) {
		return this.get(`things/${id}/images`);
	}

	async getItemComments(id) {
		return this.get(`things/${id}/comments`);
	}

	async getItemFiles(id) {
		return this.get(`things/${id}/files`);
	}
}
module.exports = ThingiverseAPI;

async function featured(_source, { id }, { dataSources }) {
	return dataSources.ThingiverseAPI.featured(id);
}

async function getItem(_source, { id }, { dataSources }) {
	return dataSources.ThingiverseAPI.getItem(id);
}

async function getItemImages(_source, { id }, { dataSources }) {
	return dataSources.ThingiverseAPI.getItemImages(id);
}

async function getItemComments(_source, { id }, { dataSources }) {
	return dataSources.ThingiverseAPI.getItemComments(id);
}

async function getItemFiles(_source, { id }, { dataSources }) {
	return dataSources.ThingiverseAPI.getItemFiles(id);
}

module.exports = {
	// hello,
	featured,
	getItem,
	getItemImages,
	getItemComments,
	getItemFiles
};

// async function features(parent, args, context) {
// 		const bearer_token = '2e1e23cf95a1271a4f21d125d037b5be';
// 		let bearer = 'Bearer ' + bearer_token;

// 		const items = fetch('https://api.thingiverse.com/featured', {
// 			headers: new Headers({
// 				'Authorization': bearer,
// 				'Content-Type': 'application/x-www-form-urlencoded'
// 			}),
// 		})
// 		.then(res => res.json())
// 		// .then((data) => {
// 		// 	this.setState({ contacts: data })
// 		// })
// 		.catch(console.log)

//   return {
//     items
//   }
// }

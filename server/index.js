const { ApolloServer } = require("apollo-server");

// Import typedef from file
const { importSchema } = require("graphql-import");
// const typeDefs = importSchema('./schema.graphql')

const Query = require("./resolvers/Query");
const Thing = require("./resolvers/Thing");
const ThingiverseAPI = require("./resolvers/ThingiverseAPI");

const resolvers = {
	Query,
	Thing
	// Mutation,
	// Subscription,
};

const typeDefs = importSchema("./server/schema.graphql");

const server = new ApolloServer({
	typeDefs,
	resolvers,
	dataSources: () => {
		return {
			ThingiverseAPI: new ThingiverseAPI()
		};
	}
});

server.listen().then(({ url }) => {
	console.log(`🚀 Server ready at ${url}`);
});
